first_name="ada"
last_name="lovelace"
full_name=f"{first_name}  {last_name}"
message=f"Hello,{full_name.title()}"
print("\tmessage")
print("\nmessage")

print(2+3)
print(3-2)
print(3*2)
print(3**3)
print(10**6)
print(0.1+0.1)
print(2*0.1)

universe_age=14_000_000_000
print(universe_age)

#zhushi

bicycles=['trek','cannondale','reline','specialized']
print(bicycles)
print(bicycles[0].title())
print(bicycles[1].title())
print(bicycles[3].title())
print(bicycles[-1].title())
message=f"My first bicycles is {bicycles[-1].title()}"
print(message)

motorcycles=['honda','yamaha','suzuki']
print(motorcycles)

motorcycles.append('ducati')
motorcycles.insert(0,'pingan')
motorcycles.insert(0,'zhonghua')
del motorcycles[0]
print(motorcycles)

popped_motorcycles=motorcycles.pop(0)
print(motorcycles)
print(popped_motorcycles)

motorcycles.remove('ducati')
print(motorcycles)

cars=['bmw','audi','toyota','subaru']
cars.sort()
print(cars)

cars.sort(reverse=True)
print(cars)

cars=['bmw','audi','toyota','subaru']
print("Here is the original list:")
print(len(cars))

cars.reverse()
print(cars)

print("\nHere is the sorted list:")
print(sorted(cars))

print("\nHere is the original list again:")
print(cars)

